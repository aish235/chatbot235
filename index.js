'use strict';

var express  = require('express');
var bodyParser   = require('body-parser');
var http         = require('http');
var config       = require('./Config');
var server       = express();
var mongoose     = require('mongoose');
let TeamInfo     = require('./API/Models/TeamInfo'); //created model loading here

let GameSchedule = require('./API/Models/GameSchedule');
const hostname = '127.0.0.1';
const port = 3000;
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
console.log(config.dbUrl);

mongoose.connect(config.dbUrl, {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
console.log("we are connected");
});

// mongoose.connect(config.dbUrl)
// mongoose.connection.on('error', function(e) {
//     console.log('Can not connect Error:>>',e);
//     process.exit();
// });

// mongoose.collection("gameschedule").findOne({}, function(err, result) {
//     if (err) throw err;
//     console.log(result.name);
// });

// mongoose.connect(config.dbUrl, { useNewUrlParser: true }, function(err, db) {
//   console.log("connected to database");
// });
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
var routes = require('./API/Routes/Routes'); //importing route
routes(server); //register the route
server.listen((process.env.PORT || 8000), function () {
    console.log("Server is up and listening on port" + process.env.PORT);
});
// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });
